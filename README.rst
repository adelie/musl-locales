=========================
 README for musl-locales
=========================
:Authors:
  * **Konstantin Pugin**, original developer
  * **A. Wilcox**, maintainer
  * **Contributors**, translations and fixes (see commit log)
:Status:
  Production




Introduction
============

This repository contains code and translation files for better locale support
for the `musl libc`_.

The **locale(1)** command displays information about the currently set locale,
or displays a list of locales available on the system.

The translation files provide localisation to musl, including libc-provided
error messages, date and time strings, and so forth.

.. _`musl libc`: https://musl.libc.org


License
```````

Translation files are provided under the MIT license.  The **locale(1)**
implementation is provided under the LGPL v3 license (LGPL-3.0-only).




Building
========

This section contains information about building the **locale(1)** command and
translations.


Requirements
````````````

This package uses the CMake build system.

To build the translation files, you will need an implementation of ``msgfmt``
and ``xgettext``.  Both GNU gettext and gettext-tiny are supported.

To build the **locale(1)** command, you will need a C compiler, the musl libc,
and an implementation of ``libintl``.  Again, GNU gettext and gettext-tiny are
both supported.


Commands
````````

To build in the same directory as the source::

    cmake .
    make
    sudo make install

Out-of-tree builds are also supported::

    cmake -Bbuild .
    make -C build
    sudo make -C build install




Included translations
=====================

This section contains information about the translations included with the
musl-locales package.  Note that "fully translated" does not mean that a given
translation is free from defects.  Feel free to open an issue or merge request
if you identify an issue in any translation, including those listed as fully
translated.


========  ===============================================
Language  Status
========  ===============================================
cs_CZ     Fully translated.
de_DE     Date/time strings only.
en_GB     Messages translated where different from en_US.
es_ES     Fully translated.
fi_FI     Date/time strings only.
fr_FR     Date/time strings only.
it_IT     Date/time strings only.
nb_NO     Date/time strings only.
nl_NL     Date/time strings only.
pt_BR     Fully translated.
pt_PT     Fully translated.
ru_RU     Fully translated.
sr_RS     Fully translated.
sv_SE     Date/time strings only.
========  ===============================================




End-user packages
=================

.. image:: https://repology.org/badge/vertical-allrepos/musl-locales.svg
   :target: https://repology.org/project/musl-locales/versions
   :alt: Repology packaging status (Adélie, Alpine, Exherbo, and more)
