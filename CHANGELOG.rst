============================
 Changelog for musl-locales
============================
:Author:
  * **A. Wilcox**, documentation writer
  * **Contributors**, code



0.1 (2022-05-07)
================

Initial release with support full support for:

* cs_CZ

* en_GB

* pt_PT

* ru_RU

* sr_RS


And date/time string translation for:

* de_DE

* es_ES

* fi_FI

* fr_FR

* it_IT

* nb_NO

* nl_NL

* pt_BR

* sv_SE
